package com.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ClientConnectionTask implements Runnable{
    private Socket socket;
    private DataInputStream in;

    public ClientConnectionTask(Socket clientSocket) throws IOException {
        this.socket = clientSocket;
        this.in = new DataInputStream(clientSocket.getInputStream());
    }

    @Override
    public void run() {
        while (true) {
            try {
                String sw = in.readUTF();
                System.out.println("Message: " + sw);
                if (sw.equals("-exit")){
                    this.socket.close();
                    break;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
