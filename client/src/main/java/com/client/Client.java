package com.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


public class Client {

    private static final Logger log = LogManager.getLogger(Client.class);

    public Client(){
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);
        log.info("com.client.Client is starting up ...");
        try {
            Socket socket = new Socket("localhost", 8080);
            System.out.println("com.client.Client successfully connected.");
            ClientConnectionTask clientConnectionTask = new ClientConnectionTask(socket);
            ClientWriteTask clientWriteTask=new ClientWriteTask(socket);
            executorService.submit(clientConnectionTask);
            executorService.submit(clientWriteTask);

        } catch (IOException e) {
            throw new RuntimeException("SWW while client processing...", e);
        }
    }
}
