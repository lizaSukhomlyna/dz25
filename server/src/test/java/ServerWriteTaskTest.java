import com.server.ServerWriteTask;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;


class ServerWriteTaskTest {

    @Test
    void copyFileTest() throws IOException {
        new ServerWriteTask().copyFile("-file D:\\JavaProjects\\ng\\dz25\\server\\src\\test\\resources\\filefortest.txt");
        Assert.assertTrue(new File("D:\\JavaProjects\\ng\\dz25\\server\\src\\test\\resources\\filefortest.txt").exists());
    }


}