import com.server.ClientData;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.net.Socket;
import java.util.Date;

class ClientDataTest {


    @Test
    void getName() {
        Assert.assertEquals("Client1",new ClientData("Client1",new Socket()).getName());
    }


    @Test
    void getSocket() {
        Socket clientSocket= new Socket();
        Assert.assertEquals(clientSocket, new ClientData("Client1",clientSocket).getSocket());

    }
}