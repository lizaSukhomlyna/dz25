package com.server;

import java.io.*;
import java.net.Socket;

import org.apache.commons.io.FileUtils;


public class ServerWriteTask implements Runnable {
    private Socket clientSocket;
    private DataInputStream in;
    private DataOutputStream out;
    public ServerWriteTask(){}

    public ServerWriteTask(Socket clientSocket) throws IOException {
        this.clientSocket = clientSocket;
        this.in = new DataInputStream(clientSocket.getInputStream());
        this.out = new DataOutputStream(clientSocket.getOutputStream());
    }

    @Override
    public void run() {
        while (true) {
            try {
                String s = in.readUTF();
                out.writeUTF(s);
                if (s.equals("-exit")) {
                    this.clientSocket.close();
                    break;
                }
                if (s.contains("-file")) {
                    copyFile(s);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    public void copyFile(String command) throws IOException {
        String path = command.split("-file ")[1];
        File srcFile = new File(path);
        File destFile = new File("D:\\JavaProjects\\ng\\dz25\\server\\src\\main\\resources");
        if (srcFile.exists()) {
            FileUtils.copyFileToDirectory(srcFile, destFile);
        } else {
            System.out.println("This file not exist");
        }
        System.out.println();
    }

}

