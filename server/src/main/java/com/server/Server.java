package com.server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Server {


    private static final Logger log = LogManager.getLogger(Server.class);
    public static LinkedList<ClientData> serverConnections = new LinkedList<>();

    public Server() throws IOException {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);
        try (ServerSocket serverSocket = new ServerSocket(8080)) {
            log.info("Server is starting up ...");
            log.debug("Server: {}", serverSocket);
            log.info("Server is waiting for a connection...");
            int i = 1;
            while (true) {
                try {
                    Socket client = serverSocket.accept();
                    executorService.submit(new ServerWriteTask(client));
                    serverConnections.add(new ClientData("Client" + i, client));
                    int finalI = i;
                    serverConnections.forEach(c -> {
                        try {
                            DataOutputStream out = new DataOutputStream(c.getSocket().getOutputStream());
                            out.writeUTF("[Server]: Client " + finalI + "успешно подключился");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                    serverConnections.stream()
                            .map(ClientData::getName)
                            .forEach(System.out::println);
                    log.info("Client successfully connected: {}", client);
                } catch (IOException e) {
                    throw new RuntimeException("SWW while server processing...", e);
                }
                i++;
            }
        }

    }

}


