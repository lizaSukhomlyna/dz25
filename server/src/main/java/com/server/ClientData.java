package com.server;

import java.net.Socket;
import java.util.Date;


public class ClientData {
    private String name;
    private Date date;
    private Socket socket;

    public ClientData(String name, Socket socket) {
        this.name = name;
        this.date = new Date();
        this.socket = socket;
    }

    public String getName() {
        return name;
    }

    public Date getDate() {
        return date;
    }

    public Socket getSocket() {
        return socket;
    }
}
